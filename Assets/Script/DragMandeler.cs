﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragMandeler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

	public void OnBeginDrag(PointerEventData eventData)
	{
	}
	public void OnDrag(PointerEventData eventData)
	{
		Quaternion newRotation = Quaternion.LookRotation(transform.position - Input.mousePosition, Vector3.forward);
		newRotation.x = 0;
		newRotation.y = 0;
		transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 8);
	}

	public void OnEndDrag(PointerEventData eventData)
	{

	}

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{


	}
	void CursorMove()
	{

	}
}
