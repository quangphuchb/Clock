﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClockAnimator : MonoBehaviour
{
    public Transform hours, minutes, seconds;

    public Transform alarm;

    private const float
        hoursToDegrees = 360f / 12f,
        minutesToDegrees = 360f / 60f,
        secondsToDegrees = 360f / 60f;
    private const float alarmToDegrees = 360f / 12f;

    private const float Day = 24 * 60;

    public bool analog;

    public List<GameObject> lstNum = new List<GameObject>();

    [SerializeField]
    GameObject wakeup;

    float _hours = 7;
    float _minutes = 41;
    float _seconds = 0;

    public Text showTime;
    // Use this for initialization
    void Start()
    {
        SetNumber();
    }

    // Update is called once per frame
    void Update()
    {

        Show();
        SetArrowAlarm();
        if (analog)
        {
            TimeSpan timespan = DateTime.Now.TimeOfDay;

            hours.localRotation = Quaternion.Euler(0, 0, (float)timespan.TotalHours * -hoursToDegrees);
            minutes.localRotation = Quaternion.Euler(0, 0, (float)timespan.TotalMinutes * -minutesToDegrees);
            seconds.localRotation = Quaternion.Euler(0, 0, (float)timespan.TotalSeconds * -secondsToDegrees);

            alarm.localRotation = Quaternion.Euler(0, 0, (float)_hours * -hoursToDegrees);
        }
        else
        {
            DateTime time = DateTime.Now;

            hours.localRotation = Quaternion.Euler(0f, 0f, time.Hour * -hoursToDegrees);
            minutes.localRotation = Quaternion.Euler(0, 0, time.Minute * -minutesToDegrees);
            seconds.localRotation = Quaternion.Euler(0, 0, time.Second * -secondsToDegrees);

            alarm.localRotation = Quaternion.Euler(0, 0, _hours * -hoursToDegrees);
        }
    }

    void SetArrowAlarm()
    {
        DateTime time = DateTime.Now;
        //float h = alarm.localRotation.eulerAngles.z / hoursToDegrees;
        //float m = alarm.localRotation.eulerAngles.z / minutesToDegrees;


        //Debug.Log(Mathf.Round(h) + ":" + m);
        Debug.Log(time.Hour + ":" + time.Minute);


        //if (Mathf.Round(h) == time.Hour && m == time.Minute)
        //{
        //    wakeup.GetComponent<Text>().text = "WAKEUP";

        //}
        //else
        //{
        //    wakeup.GetComponent<Text>().text = "";
        //}

        if (time.Hour == _hours && time.Minute == _minutes && time.Second == _seconds)
        {
            wakeup.GetComponent<Text>().text = "WAKEUP";
            StartCoroutine(StopAlarm());
        }
    }
    IEnumerator StopAlarm()
    {
        yield return new WaitForSeconds(5f);
        wakeup.GetComponent<Text>().text = "";
    } 
    void Show()
    {
        if (_hours < 10 && _minutes < 10 && _seconds < 10)
        {
            showTime.text = "0" + _hours + ":" + "0" + _minutes + ":" + "0" + _seconds;
        }
        else if (_hours < 10 && _minutes < 10)
        {
            showTime.text = "0" + _hours + ":" + "0" + _minutes + ":" + _seconds;
        }
        else if (_hours < 10 && _seconds < 10)
        {
            showTime.text = "0" + _hours + ":" + _minutes + ":" + "0" + _seconds;
        }
        else if (_hours < 10)
        {
            showTime.text = "0" + _hours + ":" + _minutes + ":" + _seconds;
        }
        else if (_minutes < 10)
        {
            showTime.text = _hours + ":" + "0" + _minutes + ":" + _seconds;
        }
        else if (_seconds < 10)
        {
            showTime.text = _hours + ":" + _minutes + ":" + "0" + _seconds;
        }
        else
        {
            showTime.text = _hours + ":" + _minutes + ":" + _seconds;
        }
    }

    public void btnUp()
    {
        if (_seconds < 59)
        {
            _seconds += 1;
        }
        else
        {
            _seconds = 0;
            if (_minutes < 59)
            {
                _minutes++;
            }
            else
            {
                _minutes = 0;
                if (_hours < 23)
                {
                    _hours++;
                }
                else
                {
                    _hours = 0;
                }
            }
        }

    }
    public void btnDown()
    {
        _seconds -= 1;

        if (_seconds <= 0)
        {
            _seconds = 0;
            if (_minutes > 0)
            {
                _seconds = 59;
                _minutes--;
            }
            else
            {
                _minutes = 0;
                _hours--;
                if (_hours < 0)
                {
                    _hours = 0;
                    _seconds = 0;
                }

            }
        }
    }
    float posY = 180;
    float posX;
    void SetNumber()
    {
        for (int n = 0; n < lstNum.Count; n++) {
            if (n <= 5)
            {
                posY -= 70;
            }
            else
            {
                posY += 70;
            }
            if (n < 3)
            {
                posX += 60;
            }
      
            else if (n > 8)
            {
                posX += 60;
            }
            else
            {
                posX -= 60;
            }

            lstNum[n].transform.localPosition = new Vector3(posX, posY, 0);
        }
       
    }
}
